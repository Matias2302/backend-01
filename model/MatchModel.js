var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var UserSchema = require('./UserModel').UserSchema
mongoose.model('User', UserSchema)

mongoose.connect('mongodb://localhost/nerdos');

var MatchSchema = new mongoose.Schema({
    name: String,
    player: {type: Schema.Types.ObjectId, ref: 'User'}
    
});

var MatchModel = mongoose.model('match', MatchSchema);

module.exports = MatchModel;
