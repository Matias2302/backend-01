var mongoose = require('mongoose')
mongoose.connect('mongodb://localhost/nerdos');

var CardSchema = new mongoose.Schema({
    name: String,
    img_url: String
});

var CardModel = mongoose.model('card', CardSchema);

module.exports = CardModel;
