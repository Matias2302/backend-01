var express = require('express');


var UserModel = require("../model/UserModel").UserModel;
var router = express.Router();

router.get('/', function (req, res) {
    UserModel.find().then(function (usuarios) {
        res.json(usuarios);
    })
});

router.get("/:id", function (req, res) {
    UserModel.findById(req.params.id).then(function (user) {
        res.json(user);
    })
})

router.put("/:id", function (req, res) {
    UserModel.findById(req.params.id).then(function (user) {
        user.name = req.body.name ? req.body.name : user.name;
        user.password = req.body.password ? req.body.password : user.password;
        user.email = req.body.email ? req.body.email : user.email;
        user.save().then(function (u) {
            res.json(u);
        })
    })
})

router.post('/', function (req, resp) {
    var user = new UserModel({
        name: req.body.name,
        password: req.body.password,
        email: req.body.email
    })
    user.save().then(function (u) {
        resp.json(u);
    })
})

router.delete("/:id", function (req, resp) {
    UserModel.deleteOne({_id: req.params.id}).then(function () {
        resp.json({});
    })

})

module.exports = router;
