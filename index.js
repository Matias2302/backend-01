var express = require('express');
var bodyParser = require('body-parser');

var userRouter = require('./controller/UserController');
var cardRouter = require('./controller/CardController')
var matchRouter = require('./controller/MatchController')

var app = express();


app.use(bodyParser.urlencoded());

app.use('/users', userRouter);
app.use('/cards', cardRouter);
app.use('/matches', matchRouter);

app.listen(3000);
